package com.gpacalc.GpaCalculator.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gpacalc.GpaCalculator.entity.GpaInput;
import com.gpacalc.GpaCalculator.entity.Student;
import com.gpacalc.GpaCalculator.entity.SubjectMarks;
import com.gpacalc.GpaCalculator.repository.GpaRepository;
import com.gpacalc.GpaCalculator.repository.StudentRepository;

@Service
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private GpaRepository gpaRepository;
	
	public Student saveStudent(Student student) {
		return studentRepository.save(student);
	}
	
	public List<Student> getAllStudent(){
		List<Student> studentList = new ArrayList();
		studentRepository.findAll().forEach(s -> studentList.add(s));
		return studentList;
	}
	
	public Optional<Student> getStudentDetails(long id) {
		return studentRepository.findById(id);
	}
	
	public Student getStudentDetailsByName(String name) {
		return studentRepository.findByStudentName(name);
	}
	
	public void deleteStudent(Student student) {
		studentRepository.deleteById(student.getStudentId());
	}

	public List<SubjectMarks> getSubjectMarks(GpaInput gpaInput) {
		List<SubjectMarks>  subjectMarksList = gpaRepository.getStudentMarks(gpaInput.getCourseId(), gpaInput.getSemester());
		return subjectMarksList;
	}

}
