package com.gpacalc.GpaCalculator.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gpacalc.GpaCalculator.entity.SubjectMarks;

@Repository
public class GpaRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<SubjectMarks> getStudentMarks(int courseId, int semester) {
		
		System.out.println("Course id : " + courseId);
		System.out.println("semester : " + semester);
		
		String sql = "select subject,marks,semester from student_grade sg, course c where c.course_id = sg.course_id and c.course_id =? and semester=? ";
		
		
		return jdbcTemplate.query(sql,new Object[]{courseId,semester},(rs, rowNum) ->
            new SubjectMarks
            (
              rs.getString(1),
              rs.getInt(2),
              rs.getInt(3)
            
            )
        );
		
	}

}
