package com.gpacalc.GpaCalculator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gpacalc.GpaCalculator.entity.GpaInput;
import com.gpacalc.GpaCalculator.entity.Student;
import com.gpacalc.GpaCalculator.entity.SubjectMarks;
import com.gpacalc.GpaCalculator.service.StudentService;

@RestController
@CrossOrigin(origins = "*")
public class GpaCalcController {
	
	@Autowired
	private StudentService service;
	
	@GetMapping("/gpa/hello")
	public String sayHello() {
		String response = "Hey...you are going to use my GPA calculator";
		return response;
	}
	
	@PostMapping("/gpa/student/add")
	public Student addNewStudent(@RequestBody Student student) {
		return service.saveStudent(student);
	}
	
	@GetMapping("/gpa/student/all")
	public List<Student> getAllStudent(){
		return service.getAllStudent();
	}
	
	@GetMapping("/gpa/student/details/{id}")
	public Student getStudentDetails(@PathVariable long id){
		return service.getStudentDetails(id).orElse(new Student());
	}
	
	@PostMapping("/gpa/student/details")
	public Student getStudentDetailsById(@RequestBody Student student){
		return service.getStudentDetails(student.getStudentId()).orElse(new Student());
	}
	
	@PostMapping("/gpa/student/detailsByName")
	public Student getStudentDetailsByName(@RequestBody Student student){
		return service.getStudentDetailsByName(student.getStudentName());
	}
	
	@PostMapping("gpa/student/deleteById")
	public void deleteStduent(@RequestBody Student student) {
		service.deleteStudent(student);
	}
	
	@PostMapping("gpa/student/edit")
	public void editStduent(@RequestBody Student student) {
		service.saveStudent(student);
	}
	
	@PostMapping("gpa/student/subjectMarks")
	public List<SubjectMarks> getSubjectMarks(@RequestBody GpaInput gpaInput) {
		return service.getSubjectMarks(gpaInput);
	}
	

}
