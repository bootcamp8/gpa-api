package com.gpacalc.GpaCalculator.entity;

public class GpaInput {
	
	private int courseId;
	private int semester;
	
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public int getSemester() {
		return semester;
	}
	public void setSemester(int semester) {
		this.semester = semester;
	}


}
