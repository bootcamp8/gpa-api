package com.gpacalc.GpaCalculator.entity;

public class SubjectMarks {
	
	private String subject;
	private int semester;
	private int marks;
	
	public SubjectMarks(String subject, int semester, int marks) {
		super();
		this.subject = subject;
		this.semester = semester;
		this.marks = marks;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public int getSemester() {
		return semester;
	}
	public void setSemester(int semester) {
		this.semester = semester;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	
	

}
